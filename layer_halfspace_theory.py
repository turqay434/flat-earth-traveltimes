import numpy as np

def Pg(n, X):

    return n * X

def PmP(n, h, X):

    p = n * X / np.sqrt(4 * h**2 - X**2)
    T = 2 * n**2 * h / np.sqrt(n**2 - p**2)

    return T

def Pn(n0, n1, h, X):

    T = np.empty(len(X), dtype=float)

    x_min = 2 * h * n1 / np.sqrt(n0**2 - n1**2)
    T_min = 2 * h * n0**2 / np.sqrt(n0**2 - n1**2)

    npts = len(X)

    for i in xrange(npts):
        x = X[i]
        if x < x_min:
            T[i] = np.nan
        else:
            T[i] = T_min + n1 * (x - x_min)

    return T





