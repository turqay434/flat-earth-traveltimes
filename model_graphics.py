import matplotlib.pyplot as plt

def plot_model_vp_vs(df):

    plt.plot(df["VP"], df["Z"], color="red", label="Vp")
    plt.plot(df["VS"], df["Z"], color="blue", label="Vs")
    plt.gca().invert_yaxis()
    plt.gca().xaxis.tick_top()
    plt.gca().xaxis.set_label_position('top')
    plt.xlabel("Velocity (km/s)")
    plt.ylabel("Depth (km)")
    plt.legend()

def plot_model_label(df, label, unit, color="blue"):

    plt.plot(df[label], df["Z"], color=color, label=label)
    plt.gca().xaxis.tick_top()
    plt.gca().xaxis.set_label_position('top')
    plt.xlabel(label + " " + unit)
    plt.ylabel("Depth (km)")
    plt.legend()

def plot_tt_curve(X, T, vred=None, color='blue'):

    if vred is None:
        plt.plot(X, T, '.', color=color)
        plt.ylabel("T (s)")
    else:
        plt.plot(X, T-X/vred, '.', color=color)
        plt.ylabel("T - X / %0.1f (s)" % vred)
    plt.xlabel("X (km)")

def plot_Xp_curve(p, X, color='blue'):

    plt.plot(p, X, '.', color=color)
    plt.ylabel("X (km)")
    plt.xlabel("p (s/km)")

def plot_Tp_curve(p, T, color='blue'):

    plt.plot(p, T, '.', color=color)
    plt.ylabel("T (s)")
    plt.xlabel("p (s/km)")

def plot_taup_curve(p, X, T, color='blue'):

    plt.plot(p, T - p * X, '.', color=color)
    plt.xlabel("p (s/km)")
    plt.ylabel("Tau (s)")


